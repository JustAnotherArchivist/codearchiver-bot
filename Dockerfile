FROM python:3.11-alpine
COPY codearchiver-bot /app/codearchiver-bot
RUN apk add --no-cache bash coreutils curl git sed zstd
RUN git clone https://gitea.arpa.li/JustAnotherArchivist/little-things.git /app/little-things/ \
 && (cd /app/little-things/ && git rev-parse HEAD)
RUN pip install --no-cache-dir codearchiver
VOLUME /data
WORKDIR /data
ENV PATH="/app/little-things:${PATH}"
CMD /app/codearchiver-bot
